#!/bin/bash
. config.sh

if [ ! -f "${TARGET_OUTPUT}/bin/agent" ];
then
    echo "Agent not found."
    exit;
fi;

echo "Start ROSCORE..."
xterm -title "ROSCORE" -e "roscore; bash" &
sleep 2

echo "Start up environment..."
xterm -title "TORCSEnvironment" -e ${EXEC_ENVIRONMENT_PLAY} &
sleep 4 &

echo "Start up postprocessor..." &
xterm -title "Postprocessor" -e "${BINARY_OUTPUT}/postprocessor" &
sleep 4 &

echo "Start agent..."
(cd ${BINARY_OUTPUT}/agent; ./agent)